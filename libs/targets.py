#! /usr/bin/env python

from datetime import datetime
from os import path
from astropy.coordinates import SkyCoord, FK5


def get_decimal_year():
    now = datetime.now().timetuple()
    return now.tm_year + now.tm_yday / 365.25


def get_target_coordinates(object_name):
    tabob_file = path.join(path.split(__file__)[0], "TABOB.TXT")
    for line in open(tabob_file):
        if line.split()[0] == object_name:
            ra_h = line.split()[1]
            ra_m = line.split()[2]
            ra_s = line.split()[3]
            dec_d = line.split()[4]
            dec_m = line.split()[5]
            dec_s = line.split()[6]
            break
    else:
        return None
    catalog_coord = SkyCoord(ra="%sh%sm%ss" % (ra_h, ra_m, ra_s),
                             dec="%sd%sm%ss" % (dec_d, dec_m, dec_s),
                             frame=FK5(equinox="J2000"))
    transormed_coord = catalog_coord.transform_to(frame=FK5(equinox="J%1.2f" % get_decimal_year()))
    return transormed_coord
