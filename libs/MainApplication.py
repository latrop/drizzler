#! /usr/bin/env python

import tkinter as Tk
import zmq
from tkinter import messagebox
import threading
import time
from .telescope import Telescope
from .targets import get_target_coordinates


class MainApplication(Tk.Frame):
    def __init__(self, *args, **kwargs):
        self.root = Tk.Tk()
        self.root.title("Drizzler")
        self.root.protocol("WM_DELETE_WINDOW", self.shutdown)
        self.root.resizable(0, 0)

        # Interface
        # Telescope status
        Tk.Label(self.root, text="Telescope:", font=("Helvetica", 12)).grid(column=0, row=0, padx=5, pady=5)
        self.telescope_status = Tk.StringVar()
        self.telescope_status.set("Not connected")
        Tk.Label(self.root, textvariable=self.telescope_status).grid(column=1, row=0)
        # Navigation
        self.object_value = Tk.StringVar()
        Tk.Label(self.root, text="Object:", font=("Helvetica", 12)).grid(column=0, row=1)
        Tk.Entry(self.root, textvariable=self.object_value, width=10).grid(column=1, row=1)
        Tk.Button(self.root, text="Go!", command=self.slew_to_object).grid(column=2, row=1)
        # Dither controls
        Tk.Label(self.root, text="Dither step:", font=("Helvetica", 12)).grid(column=0, row=2)
        self.dither_step = Tk.StringVar()
        self.dither_step.set(30)
        Tk.Entry(self.root, textvariable=self.dither_step, width=10).grid(column=1, row=2)
        self.do_dithering = Tk.IntVar()
        self.do_dithering.set(1)
        Tk.Checkbutton(self.root, variable=self.do_dithering).grid(column=2, row=2)
        # Object coordinates
        Tk.Label(self.root, text="Object coords:", font=("Helvetica", 12)).grid(column=0, row=3)
        self.object_coords_ra = Tk.StringVar()
        self.object_coords_ra.set("ra=--h --m --s")
        self.object_coords_dec = Tk.StringVar()
        self.object_coords_dec.set("dec=--d --m --s")
        Tk.Label(self.root, textvariable=self.object_coords_ra, width=15,
                 font=("Helvetica", 12), anchor=Tk.W, justify=Tk.LEFT).grid(column=1, row=3, columnspan=2)
        Tk.Label(self.root, textvariable=self.object_coords_dec, width=15,
                 font=("Helvetica", 12), anchor=Tk.W, justify=Tk.LEFT).grid(column=1, row=4, columnspan=2)

        # Telescope coordinates
        Tk.Label(self.root, text="Telescope coords:", font=("Helvetica", 12)).grid(column=0, row=5)
        self.telescope_coords_ra = Tk.StringVar()
        self.telescope_coords_ra.set("ra=--h --m --s")
        self.telescope_coords_dec = Tk.StringVar()
        self.telescope_coords_dec.set("dec=--d --m --s")
        Tk.Label(self.root, textvariable=self.telescope_coords_ra, width=15,
                 font=("Helvetica", 12), anchor=Tk.W, justify=Tk.LEFT).grid(column=1, row=5, columnspan=2)
        Tk.Label(self.root, textvariable=self.telescope_coords_dec, width=15,
                 font=("Helvetica", 12), anchor=Tk.W, justify=Tk.LEFT).grid(column=1, row=6, columnspan=2)

        # Connect to telescope
        self.telescope = Telescope(self)

        # ZMQ socket
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PAIR)
        self.socket.setsockopt(zmq.RCVTIMEO, 3000)
        self.socket.connect("tcp://localhost:5556")

        # Listen for signals
        self.do_listen = True
        threading.Thread(target=self.listen).start()
        self.root.mainloop()

    def show_coords(self):
        while self.do_listen:
            print()
            print("RA:", self.telescope.send_command(":GR#", True))
            print("DEC:", self.telescope.send_command(":GD#", True))
            print()
            time.sleep(1.0)

    def shutdown(self):
        self.do_listen = False
        self.telescope.do_connect = False
        if self.telescope.connected is True:
            self.telescope.close
        else:
            self.telescope.do_connect = False
        self.root.destroy()

    def slew_to_object(self):
        coordinates = get_target_coordinates(self.object_value.get())
        h, m, s = coordinates.ra.hms.h, coordinates.ra.hms.m, coordinates.ra.hms.s
        self.object_coords_ra.set("ra:%02ih %02im %02is" % (h, m, s))
        d, m, s = coordinates.dec.dms.d, coordinates.dec.dms.m, coordinates.dec.dms.s
        self.object_coords_dec.set("dec:%02id %02im %02is" % (d, m, s))
        if coordinates is None:
            messagebox.showwarning(parent=self.root, title="Warning",
                                   message="Object not found!")
            return
        self.telescope.set_target_coords(coordinates)
        self.telescope.slew()

    def listen(self):
        while self.do_listen:
            try:
                msg = self.socket.recv().decode()
                print(msg)
            except zmq.error.Again:
                time.sleep(0.05)
                continue
            if self.do_dithering.get() == 1:
                self.telescope.dither()
