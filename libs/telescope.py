#! /usr/bin/env python

import time
from serial import Serial
from serial import SerialException
import threading
import math
import random


class Telescope(object):
    def __init__(self, window):
        self.window = window
        self.window.telescope_status.set("Connecting...")
        self.dither_iteration = 0
        self.current_dec = 0.0
        self.current_ra = 0.0
        self.do_connect = True
        self.connected = False
        self.last_delta_change = 0
        self.current_slew_rate = None
        threading.Thread(target=self.connect).start()

    def connect(self):
        while self.do_connect:
            try:
                self.device = Serial("COM1", baudrate=9600, timeout=0.5)
                # We have opened the com port, but this does not mean that the
                # telescope is woring now. Let's send some command to see if we are
                # indeed communicating with the telescope
                response = self.send_command(":GC#", get_response=True)
                if len(response) == 0:
                    continue
                self.window.telescope_status.set("Connected")
                print("RA:", self.send_command(":GR#", True))
                print("DEC:", self.send_command(":GD#", True))
                self.connected = True
                break
            except SerialException:
                time.sleep(5)

    def send_command(self, command, get_response=False):
        # self.device.flushOutput()
        self.device.read(self.device.out_waiting)
        self.device.write(str.encode(command))
        if get_response is True:
            response = []
            while 1:
                new_byte = self.device.read(size=1).decode("latin_1")
                if (new_byte == "#") or (new_byte == ""):
                    break
                response.append(new_byte)
            return "".join(response)
        return ""

    def get_telescope_dec(self):
        dec_string = self.send_command(":GD#", True)
        degrees, mins_and_secs = dec_string.split(u"\xdf")
        mins, secs = mins_and_secs.split(":")
        dec_value_degrees = abs(float(degrees)) + float(mins) / 60 + float(secs) / 3600
        if float(degrees) < 0:
            dec_value_degrees *= -1
        return dec_value_degrees, (int(degrees), int(mins), int(secs))

    def get_telescope_ra(self):
        ra_string = self.send_command(":GR#", True)
        hours, mins, secs = ra_string.split(":")
        hours_decimal = float(hours) + float(mins) / 60 + float(secs) / 3600
        degrees_decimal = 15 * hours_decimal
        return degrees_decimal, (int(hours), int(mins), int(secs))

    def set_target_coords(self, coordinates):
        """
        Coordinates are in astropy skycoord format.
        """
        # Set RA
        ra_h = int(coordinates.ra.hms.h)
        ra_m = coordinates.ra.hms.m
        ra_s = coordinates.ra.hms.s
        command = ":Sr%02i:%02i:%02i#" % (ra_h, ra_m, ra_s)
        self.send_command(command)

        # Set DEC
        dec_d = int(coordinates.dec.dms.d)
        dec_m = abs(int(coordinates.dec.dms.m))
        dec_s = abs(int(coordinates.dec.dms.s))
        command = ":Sd%+03i*%02i:%02i#" % (dec_d, dec_m, dec_s)
        self.send_command(command)

        # Store current coordinates
        self.object_dec = coordinates.dec.deg
        self.object_ra = coordinates.ra.deg

    def slew(self):
        command = ":MS#"
        self.send_command(command)
        self.dither_iteration = 0

    def dither(self):
        if self.dither_iteration == 0:
            # We are dithering the telescope for the first time now, so the
            # image is centered on the object and we can take current coordinates
            # as fiducual coordinates of the object around which we will wander
            self.get_telescope_ra()
            self.get_telescope_dec()
            time.sleep(0.1)
            self.object_ra, ra_hms = self.get_telescope_ra()
            self.object_dec, dec_dms = self.get_telescope_dec()
            self.window.object_coords_ra.set("ra:%02ih %02im %02is" % ra_hms)
            self.window.object_coords_dec.set("dec:%02id %02im %02is" % dec_dms)
        base_step = float(self.window.dither_step.get())
        # Find random shift around the fiducial object coordinates at which
        # we will end up after the current dithering.
        dRA_pix = random.uniform(-base_step, base_step)
        dDEC_pix = random.uniform(-base_step, base_step)
        dRA_deg = 1.34 * dRA_pix / (3600 * math.cos(math.radians(abs(self.object_dec))))
        dDEC_deg = 1.34 * dDEC_pix / 3600
        ra_final = self.object_ra + dRA_deg
        dec_final = self.object_dec + dDEC_deg
        # Now find a shift we need to make to reach the desired coordinates
        current_ra, ra_hms = self.get_telescope_ra()
        current_dec, dec_dms = self.get_telescope_dec()
        ra_shift = ra_final - current_ra
        dec_shift = dec_final - current_dec
        self.window.telescope_coords_ra.set("ra:%02ih %02im %02is" % ra_hms)
        self.window.telescope_coords_dec.set("dec:%02id %02im %02is" % dec_dms)

        # Suppose we have a second for movement, so the slew rate should be
        # equal to ra_shift and dec_shift degrees per second.
        if ra_shift < 0:
            # Compensate sky rotation
            self.send_command(":RA%1.5f#" % (1.333*abs(ra_shift - 0.004)), False)
        else:
            self.send_command(":RA%1.5f#" % (1.333*abs(ra_shift)), False)

        # Compensate the backlash in delta
        if (self.dither_iteration != 0) and (math.copysign(1, self.last_delta_change) != math.copysign(1, dec_shift)):
            # We are moving in the opposite direction in comparison with the las time
            dec_shift += math.copysign(25.0/3600, dec_shift)

        self.send_command(":RE%1.5f#" % abs(dec_shift), False)
        # Start moving
        if dec_shift > 0:
            # Going north along DEC axis
            self.send_command(":Mn#")
        else:
            # Going south
            self.send_command(":Ms#")
        if ra_shift > 0:
            # Going east
            self.send_command(":Me#")
        else:
            # Going west
            self.send_command(":Mw#")

        # Wait for one second
        time.sleep(0.75)
        # Halt movement
        self.send_command(":Q#")
        # Compensate the backlash in alpha
        if ra_shift > 0:
            self.send_command(":RA0.008#", False)
            self.send_command(":Mw#")
            time.sleep(0.25)
            self.send_command(":Q#")
        self.send_command(":RC#")
        self.dither_iteration += 1
        self.last_delta_change = dec_shift

    def close(self):
        self.send_command(":Q#")  # Just in case send halt command
        self.device.close()
